package Part2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int evaluation1, evaluation2, evaluation3;

        evaluation1 = scanner.nextInt();
        evaluation2 = scanner.nextInt();
        evaluation3 = scanner.nextInt();

        if (evaluation1 > evaluation2 && evaluation2 > evaluation3) {
            System.out.println("Петя, пора трудиться!");
        } else {
            System.out.println("Петя молодец!");
        }
    }
}
