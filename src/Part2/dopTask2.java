package Part2;

import java.util.Scanner;

public class dopTask2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String stoneInMailPackage = "камни";
        String dangerousInMailPackage = "запрещенная продукция";
        boolean checkDangerous = false;
        boolean checkStone = false;

        String mailPackage = scanner.nextLine();

        if (mailPackage.contains(stoneInMailPackage)) {
            checkStone = true;
        }
        if (mailPackage.contains(dangerousInMailPackage)) {
            checkDangerous = true;
        }
        if (checkStone && checkDangerous) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (checkDangerous) {
            System.out.println("в посылке запрещенная продукция");
        } else if (checkStone) {
            System.out.println("камни в посылке");
        } else {
            System.out.println("все ок");
        }
    }
}
