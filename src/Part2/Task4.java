package Part2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int day_number, remnant;

        day_number = scanner.nextInt();

        if (day_number == 6 || day_number == 7) {
            System.out.println("Ура, выходные!");
        } else {
            remnant = 6 - day_number;
            System.out.println(remnant);
        }
    }
}
