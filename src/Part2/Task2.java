package Part2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int coordinateX, coordinateY;

        coordinateX = scanner.nextInt();
        coordinateY = scanner.nextInt();

        if (coordinateX > 0 && coordinateY > 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
