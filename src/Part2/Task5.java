package Part2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double a, b, c, d;

        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();

        d = Math.pow(b, 2) - 4 * a * c;

        if (d > 0) {
            System.out.println("Решение есть");
        } else {
            System.out.println("Решения нет");
        }
    }
}
