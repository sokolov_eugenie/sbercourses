package Part2;

import java.util.Scanner;

public class dopTask1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String password = scanner.next();

        boolean numberPresent = false;
        boolean upperCasePresent = false;
        boolean lowerCasePresent = false;
        boolean specialCharacterPresent = false;

        String checkSpecial = "_*-";

        for (int i = 0; i < password.length(); ++i) {
            char currentCharacter = password.charAt(i);
            if (Character.isDigit(currentCharacter)) {
                numberPresent = true;
            } else if (Character.isUpperCase(currentCharacter)) {
                upperCasePresent = true;
            } else if (Character.isLowerCase(currentCharacter)) {
                lowerCasePresent = true;
            } else if (checkSpecial.contains(String.valueOf(currentCharacter))) {
                specialCharacterPresent = true;
            }
        }
        if (numberPresent  && upperCasePresent && lowerCasePresent && specialCharacterPresent && password.length() >= 8){
            System.out.println("пароль надежный");
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}
