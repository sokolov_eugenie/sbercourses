package Part2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double x;

        x = scanner.nextInt();
        x = Math.abs(x);

        System.out.println(Math.sin(x) * Math.sin(x) + Math.cos(x) * Math.cos(x) - 1);

        if ((1 - Math.cos(2 * x)) / 2 + (1 + Math.cos(2 * x)) / 2 - 1 == 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
