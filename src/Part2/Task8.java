package Part2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String one;
        int tmp;

        one = scanner.nextLine();

        tmp = one.lastIndexOf(' ');

        System.out.println(one.substring(0, tmp));
        System.out.println(one.substring(++tmp));
    }
}
