package Part2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String one = scanner.next();
        String two = scanner.nextLine();

        System.out.println(one);
        System.out.println(two.trim());
    }
}
