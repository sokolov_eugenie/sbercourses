package Part1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int num1, num2;
        double result;

        num1 = scanner.nextInt();                // вводим исходные данные с клавиатуры
        num2 = scanner.nextInt();

        result = Math.sqrt((Math.pow(num1, 2) + Math.pow(num2, 2))/2); // вычисляем среднее квадратическое

        System.out.println(result);
    }
}
