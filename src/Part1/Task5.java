package Part1;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double inch, centimetre;

        inch = scanner.nextDouble();

        centimetre = inch * 2.54;

        System.out.println(centimetre);
    }
}
