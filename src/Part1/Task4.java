package Part1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double second, minute, hour;

        second = scanner.nextInt();

        hour = second / 3600; // вычисялем кол-во часов
        minute = (second / 60 % 60); // вычисляем кол-во минут

        System.out.print((int)hour);
        System.out.print(" " + (int)minute);
    }
}
