package Part1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        final double KilometerInMile = 1.60934;

        int count;
        double mile;

        count = scanner.nextInt();

        mile = count / KilometerInMile;

        System.out.println(mile);
    }
}
